# Metodología forense para Android 

Por diferentes razones técnicas, es más complejo hacer un análisis forense de un teléfono Android. 

Actualmente MVT permite realizar dos comprobaciones diferentes en un teléfono Android: 

* Descargar APK instalados para analizarlos 
* Extraer copias de seguridad de Android para buscar SMS sospechosos 
