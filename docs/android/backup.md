# Comprobación de SMS desde la copia de seguridad de Android 

Algunos ataques contra teléfonos Android se realizan mediante el envío de enlaces maliciosos por SMS. La función de copia de seguridad de Android no permite recopilar mucha información que pueda ser interesante para un análisis forense, pero se puede utilizar para extraer SMS y verificarlos con MVT. 

Para hacerlo, debe conectar su dispositivo Android a su computadora. Luego, deberá [habilitar la depuración de USB](https://developer.android.com/studio/debug/dev-options#enable>) en el dispositivo Android. 

Si es la primera vez que se conecta a este dispositivo, deberá aprobar las claves de autenticación mediante un mensaje que aparecerá en su dispositivo Android. 

Posteriormente, puede usar [adb](https://www.xda-developers.com/install-adb-windows-macos-linux/) para extraer la copia de seguridad para SMS solo con el siguiente comando: 

```bash
adb backup com.android.providers.telephony
```

Tendrá que aprobar la copia de seguridad en el teléfono y probablemente ingresar una contraseña para cifrar la copia de seguridad. La copia de seguridad se almacenará en un archivo llamado  `backup.ab`.

Deberá utilizar [*Android Backup Extractor*](https://github.com/nelenkov/android-backup-extractor) para convertirlo a un formato de archivo legible. Asegúrese de que Java esté instalado en su sistema y use el siguiente comando: 

```bash
java -jar ~/Download/abe.jar unpack backup.ab backup.tar
tar xvf backup.tar
```

(Si la copia de seguridad está encriptada, Android Backup Extractor le pedirá la contraseña). 

Luego puede extraer SMS que contengan enlaces con MVT:

```bash
$ mvt-android check-backup --output . .
16:18:38 INFO     [mvt.android.cli] Checking ADB backup located at: .
         INFO     [mvt.android.modules.backup.sms] Running module SMS...
         INFO     [mvt.android.modules.backup.sms] Processing SMS backup
                  file at ./apps/com.android.providers.telephony/d_f/000
                  000_sms_backup
16:18:39 INFO     [mvt.android.modules.backup.sms] Extracted a total of
                  64 SMS messages containing links
```

A través del argumento `--iocs` puede especificar un archivo [STIX2](https://oasis-open.github.io/cti-documentation/stix/intro) definiendo una lista de indicadores maliciosos para revisar los registros extraídos de la copia de seguridad por mvt. Cualquier coincidencia se resaltará en el terminal. 
