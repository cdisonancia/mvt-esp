# Descarga de APKs desde un teléfono Android 

Para usar `mvt-android` necesitas conectar tu dispositivo Android a tu computadora. Luego, deberá [habilitar la depuración de USB](https://developer.android.com/studio/debug/dev-options#enable>) en el dispositivo Android.

Si es la primera vez que se conecta a este dispositivo, deberá aprobar las claves de autenticación mediante un mensaje que aparecerá en su dispositivo Android. 

Ahora puedes ejecutar `mvt-android` y especificar el comando `download-apks` la ruta a la carpeta donde desea almacenar los datos extraídos: 

```bash
mvt-android download-apks --output /direccion/de/carpeta
```

Opcionalmente, puede decidir habilitar las búsquedas del hash SHA256 de todos los APKs extraídos en [VirusTotal](https://www.virustotal.com) y/o [Koodous](https://koodous.com). Si bien estas búsquedas no brindan una evaluación concluyente sobre todos los APKs extraídos, pueden resaltar los malwares conocidos: 

```bash
mvt-android download-apks --output /direccion/de/carpeta --virustotal
mvt-android download-apks --output /direccion/de/carpeta --koodous
```

O, para iniciar todas las búsquedas disponibles: 

```bash
mvt-android download-apks --output /direccion/de/carpeta --all-checks
```
