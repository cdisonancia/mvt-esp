El uso de Docker simplifica la instalación inmediata de todas las dependencias y herramientas necesarias (incluidas las versiones más recientes de [libimobiledevice](https://libimobiledevice.org)).

Instale Docker siguiendo la [documentación oficial](https://docs.docker.com/get-docker/).

Una vez instalado, puede clonar el repositorio de MVT y crear la imagen de Docker:

```bash
git clone https://github.com/mvt-project/mvt.git
cd mvt
docker build -t mvt .
```

Prueba si la imagen se creó correctamente:

```bash
docker run -it mvt
```

Si un mensaje se genera correctamente, puede cerrarlo con `exit`.

Si desea utilizar MVT para probar un dispositivo Android, deberá habilitar el acceso del contenedor a los dispositivos USB del computador. Puede hacerlo habilitando `--privileged` y montando el dispositivo USB como volumen:

```bash
docker run -it --privileged -v /dev/bus/usb:/dev/bus/usb mvt
```

**Tenga en cuenta:** El parámetro `--privileged` generalmente se considera un riesgo de seguridad. Si desea obtener más información sobre esto, consulte [esta explicación sobre filtraciones de contenedores](https://blog.trailofbits.com/2019/07/19/understanding-docker-container-escapes/), ya que brinda acceso a todo el sistema.

parameter is generally regarded as a security risk. If you want to learn more about this check out [this explainer on container escapes](https://blog.trailofbits.com/2019/07/19/understanding-docker-container-escapes/) as it gives access to the whole system.

Las versiones recientes de Docker proporcionan un parámetro `--device` que permite especificar un dispositivo USB preciso sin habilitar --privileged:

```bash
docker run -it --device=/dev/<your_usb_port> mvt
```
