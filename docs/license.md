# Licencia MVT

El propósito de MVT es facilitar el ***análisis forense consentido*** de dispositivos de aquellos que podrían ser blanco de ataques sofisticados de software espía pata dispositivos móviles, especialmente miembros de la sociedad civil y comunidades marginadas. No queremos que MVT permita violaciones de privacidad de personas que no dan su consentimiento. Por lo tanto, el objetivo de esta licencia es prohibir el uso de MVT (y cualquier otro software con la misma licencia) con fines de *análisis forense agresivo*.

Para lograr esto, MVT se lanza bajo una adaptación de la [Licencia Pública Mozilla Public v2.0](https://www.mozilla.org/MPL). Esta licencia modificada incluye una nueva cláusula 3.0, "Restricción de uso consensual" que permite el uso del software con licencia (y cualquier "Trabajo más grande" derivado de él) exclusivamente con el consentimiento explícito de las personas cuyos datos se extraen y/o son analizados ("Propietario de los datos"). 

**Tenga en cuenta:** debido a que esta licencia impone algunas restricciones de uso, el software que la utiliza infringe la *"libertad 0"* de la [*"Definición de Software Libre"*](https://www.gnu.org/philosophy/free-sw.es.html) de la Free Software Foundation y, por lo tanto, no puede considerarse "Software libre" según la FSF. De manera similar, podría infringir los criterios de *"No discriminación contra los campos de actividad"* en la [*"Definición de código abierto"*](https://opensource.org/osd) de la **Iniciativa de Código Abierto** (ICA), por lo que el software que utiliza esta licencia también puede no considerarse "Código abierto" según la ICA. 

[Leer la licencia](https://github.com/mvt-project/mvt/blob/main/LICENSE)
