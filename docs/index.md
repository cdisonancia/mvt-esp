<p align="center">
     <img src="./mvt.png" width="300" />
</p>

# Kit de herramientas de verificación móvil

El Kit de herramientas de verificación móvil (MVT) es una herramienta para facilitar el [Análisis Forense Consentido](introduction.md#consensual-forensics) de dispositivos Android y iOS, con el fin de identificar rastros de compromiso de infección.

En esta documentación encontrarás instrucciones sobre cómo instalar y ejecutar los comandos `mvt-ios` y `mvt-android`, y orientación sobre cómo interpretar los resultados extraídos.

## Recursos

[:fontawesome-brands-python: Paquetes Python](https://pypi.org/project/mvt){: .md-button .md-button--primary } [:fontawesome-brands-github: GitHub](https://github.com/mvt-project/mvt){: .md-button }
