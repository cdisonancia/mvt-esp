# Instalación

Antes de continuar, ten en cuenta que MVT requiere Python 3.6+ para ejecutarse. Si bien debería estar disponible en la mayoría de los sistemas operativos, asegúrate de hacerlo antes de continuar.

## Dependencias en Linux

Primero instala algunas dependencias básicas que serán necesarias para construir todas las herramientas necesarias:

```bash
sudo apt install python3 python3-pip libusb-1.0-0 sqlite3
```

*libusb-1.0-0* no es necesario si solo tiene la intención de usar `mvt-ios` y no `mvt-android`.

## Dependencias en Mac

La ejecución de MVT en Mac requiere la instalación de Xcode y de [homebrew](https://brew.sh).

Para instalar dependencias usa:

```bash
brew install python3 libusb sqlite3
```

*libusb* no es necesario si solo tiene la intención de usar `mvt-ios` y no `mvt-android`.

## Instalando MVT

Si no lo has hecho, puedes agregar esto a tu archivo `.bashrc` o `.zshrc`  para agregar binarios de Pypi instalados localmente en tu `$PATH`:

```bash
export PATH=$PATH:~/.local/bin
```

Entonces puedes instalar MVT directamente desde [pypi](https://pypi.org/project/mvt/)

```bash
pip3 install mvt
```

O desde el código fuente::

```bash
git clone https://github.com/mvt-project/mvt.git
cd mvt
pip3 install .
```

Ahora deberías tener instaladas las utilidades `mvt-ios` y `mvt-android`, ejecutables desde el terminal.
