# Indicadores de Compromiso (IdC)

MVT utiliza Expresión Estructurada de Información de Amenazas ([STIX o Structured Threat Information Expression](https://oasis-open.github.io/cti-documentation/stix/intro.html)) para identificar posibles rastros de compromiso. 

Estos indicadores de compromiso están contenidos en un archivo con una estructura particular de [JSON](https://en.wikipedia.org/wiki/JSON) con las extensiones `.stix2` o `.json`. 

Puede indicar una ruta a un archivo de indicadores STIX2 al verificar las copias de seguridad del iPhone o los volcados del sistema de archivos. Por ejemplo: 

```bash
mvt-ios check-backup --iocs ~/ios/malware.stix2 --output /path/to/iphone/output /path/to/backup
```

O, con datos de una copia de seguridad de Android: 

```bash
mvt-android check-backup --iocs ~/iocs/malware.stix2 /path/to/android/backup/
```

Después de extraer los datos forenses de un dispositivo, también puede compararlos con cualquier archivo STIX2 que indique: 

```bash
mvt-ios check-iocs --iocs ~/iocs/malware.stix2 /path/to/iphone/output/
```

Si está buscando indicadores de compromiso para una pieza específica de un atacante o de un malware, pregunte a los investigadores que tengan la experiencia relevante para un archivo STIX. 

## Repositorios conocidos de IdC STIX2 

- El [repositorio de investigaciones de Amnistía Internacional](https://github.com/AmnestyTech/investigations) contiene IdC con formato STIX para: 
	- [Pegasus](https://en.wikipedia.org/wiki/Pegasus_(spyware)) ([STIX2](https://raw.githubusercontent.com/AmnestyTech/investigations/master/2021-07-18_nso/pegasus.stix2))

[Abra una incidencia](https://github.com/mvt-project/mvt/issues/) para sugerir nuevas fuentes de IdC con formato STIX.
