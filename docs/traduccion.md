# Nota de traducción

Esta traducción, realizada por el Colectivo Disonancia, es una versión en español de la documentación del programa MVT (*Mobile Verification Toolkit*), el cual es una herramienta de **Análisis Forense Consentido** para detectar Pegasus en sistemas móviles; iOS y Android.

## Repositorios

El programa se encuentra en:
[https://github.com/mvt-project/mvt](https://github.com/mvt-project/mvt)

Y la documentación oficial aquí:
[https://docs.mvt.re/en/latest/](https://docs.mvt.re/en/latest/)

La ubicación de esta traducción está en este repositorio:
[https://cdisonancia.gitlab.io/mvt-esp](https://cdisonancia.gitlab.io/mvt-esp)

## Colectivo Disonancia

Somos una organización política dedicada a promover Autonomía tecnológica, Autodefensa Digital y crítica del Control Social. Compartimos herramientas y propuestas para la colectivización y la libertad en red 🚩🏴

## Recursos
[Sitio Disonancia](https://colectivodisonancia.net){ .md-button } [:fontawesome-brands-gitlab: GitLab](https://gitlab.com/cdisonancia/mvt-esp){: .md-button }


