# Introducción

El kit de herramientas de verificación móvil (MVT) es una colección de utilidades diseñadas para facilitar el análisis forense consentido de dispositivos iOS y Android con el fin de identificar alguna señal de compromiso. Las capacidades de MVT evolucionan continuamente, pero algunas de sus características clave incluyen:

- Descifrado de las copias de seguridad de iOS encriptadas.
- Procesar y analizar registros de numerosos sistemas iOS y bases de datos de aplicaciones, registros y analítica del sistema.
- Extracción de las aplicaciones instaladas de los dispositivos Android..
- Extraer información de diagnóstico de dispositivos Android a través del protocolo adb.
- Comparar los registros extraídos con una lista proporcionada de indicadores maliciosos en formato STIX2.
- Generar registros JSON de datos extraídos y registros JSON separados de todos los rastros maliciosos detectados.
- Generar una línea de tiempo cronológica unificada de registros extraídos, junto con una línea de tiempo de todos los rastros maliciosos detectados.

## Análisis Forense Consentido

Si bien MVT es capaz de extraer y procesar varios tipos de registros muy personales que normalmente se encuentran en un teléfono móvil (como historial de llamadas, mensajes SMS y WhatsApp, etc.), esto está destinado a ayudar a identificar posibles vectores de ataque, como mensajes SMS maliciosos que conducen a la explotación.

El propósito de MVT no es facilitar el análisis forense agresivo de los dispositivos de las personas que no den su consentimiento. El uso de MVT y productos derivados para extraer y/o analizar datos provenientes de dispositivos utilizados por personas que no den su consentimiento al procedimiento está explícitamente prohibido en la [licencia](license.md).