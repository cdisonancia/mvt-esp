# Instalar libimobiledevice

Antes de continuar con la adquisición de dispositivos iOS, recomendamos instalar la librería [libimobiledevice](https://www.libimobiledevice.org/). Estas serán útiles al extraer registros de fallos y generar copias de seguridad de iTunes. Debido a que las utilidades y sus bibliotecas están sujetas a cambios frecuentes en respuesta a las nuevas versiones de iOS, es posible que desees considerar compilar las utilidades libimobiledevice a partir de las fuentes. De lo contrario, si está disponible, puedes intentar instalar los paquetes disponibles en tu distribución:

```bash
sudo apt install libimobiledevice-utils
```

En Mac, puedes intentar instalarlo desde brew:

```bash
brew install --HEAD libimobiledevice
```

Si tienes una versión razonablemente reciente de libimobiledevice en su administrador de paquetes, podría funcionar directamente. Intenta conectar tu dispositivo iOS a tu computadora a través de USB y ejecuta:

```bash
ideviceinfo
```

Si encuentras problemas inesperados, desinstala los paquetes e intenta compilar libimobiledevcice a partir de las fuentes.

## Compilar libimobiledevice desde la fuente

!!! warning 
    Las siguientes instrucciones requieren un trabajo mayor. La instalación desde la fuente requiere varios pasos, y es probable que algunos no estén registrados aquí, por lo que podría no funcionarte. Es probable que tengas que probar o jugar un poco antes de hacerlo bien.

Asegúrate de haber desinstalado todas las herramientas libimobiledevice de tu administrador de paquetes:

```bash
sudo apt remove --purge libimobiledevice-utils libimobiledevice-dev libimobiledevice6 libplist-dev libplist3 libusbmuxd-dev libusbmuxd-tools libusbmuxd4 libusbmuxd6 usbmuxd
```

Primero necesitas instalar [libplist](https://github.com/libimobiledevice/libplist). Luego puedes instalar [libusbmuxd](https://github.com/libimobiledevice/libusbmuxd).

Ahora deberías poder descargar e instalar la librería actual de herramientas [https://github.com/libimobiledevice/libimobiledevice](https://github.com/libimobiledevice/libimobiledevice).

Ahora también puedes compilar e instalar [usbmuxd](https://github.com/libimobiledevice/usbmuxd).

## Asegurándose de que todo funcione bien.

Una vez que las herramientas de idevice estén disponibles, puedes verificar si todo funciona bien conectando tu dispositivo iOS y ejecutando:

```bash
ideviceinfo
```

Esto debería generar muchos detalles sobre el dispositivo iOS conectado. Si estás conectando el dispositivo a tu computadora portátil por primera vez, deberás desbloquear e ingresar el código PIN en el dispositivo móvil. Si se notifica que no hay ningún dispositivo conectado y el dispositivo móvil está enchufado a través del cable USB, es posible que debas hacer esto primero, aunque normalmente el emparejamiento se realiza automáticamente al conectar el dispositivo:

```bash
sudo usbmuxd -f -d
idevicepair pair
```

Nuevamente, te pedirá desbloquear el teléfono e ingresar el código PIN.
