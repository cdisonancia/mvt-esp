# Verificar un volcado del sistema de archivos con `mvt-ios`

Cuando esté listo, puede continuar ejecutando `mvt-ios` sobre el volcado del sistema de archivos o el punto de montaje: 

    $ mvt-ios check-fs --help
    Usage: mvt-ios check-fs [OPTIONS] DUMP_PATH

      Extract artifacts from a full filesystem dump

    Options:
      -i, --iocs PATH     Path to indicators file
      -o, --output PATH   Specify a path to a folder where you want to store JSON
                          results

      -f, --fast          Avoid running time/resource consuming features
      -l, --list-modules  Print list of available modules and exit
      -m, --module TEXT   Name of a single module you would like to run instead of
                          all

      --help              Show this message and exit.

A continuación se muestra un ejemplo de uso básico de `check-fs`:

```bash
mvt-ios check-fs /path/to/filesystem/dump/ --output /path/to/output/
```

Este comando creará algunos archivos JSON que contienen los resultados de la extracción. Si no especifica una opción con  `--output`, mvt-ios simplemente procesará los datos sin almacenar los resultados en el disco.

Through the `--iocs` argument you can specify a [STIX2](https://oasis-open.github.io/cti-documentation/stix/intro) file defining a list of malicious indicators to check against the records extracted from the backup by mvt. Any matches will be highlighted in the terminal output as well as saved in the output folder using a "*_detected*" suffix to the JSON file name.

A través del argumento `--iocs`, puede especificar un archivo [STIX2](https://oasis-open.github.io/cti-documentation/stix/intro) definiendo una lista de indicadores maliciosos para comparar con los registros extraídos de la copia de seguridad con mvt. Cualquier coincidencia se resaltará en la salida del terminal y se guardará en la carpeta de salida con un sufijo "*_detected*" en el nombre del archivo JSON.