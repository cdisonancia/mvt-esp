# Volcando el sistema de archivos 

Si bien la copia de seguridad de iTunes proporciona una gran cantidad de bases de datos y datos de diagnóstico muy útiles, en algunos casos es posible que desee hacer jailbreak al dispositivo y realizar un volcado completo del sistema de archivos. En ese caso, debería revisar [checkra1n](https://checkra.in/), que proporciona una forma sencilla de obtener root en los modelos de iPhone más recientes. 

!!! warning
    Antes de comprobar cualquier dispositivo, asegúrese de realizar una copia de seguridad completa y de estar preparado para realizar un restablecimiento completo de fábrica antes de restaurarlo. Incluso después de usar el "Restore System" de checkra1n, todavía quedan algunos rastros del jailbreak en el dispositivo y las [aplicaciones con anti-jailbreak podrán detectarlos](https://github.com/checkra1n/BugTracker/issues/279) y dejar de funcionar. 

Después de haber desbloqueado el dispositivo, debería poder acceder al teléfono a través de *ssh*. Para hacer esto, normalmente necesitará usar iproxy, que en los sistemas Debian/Ubuntu se puede instalar con `libusbmuxd-tools`. Ejecute el comando: 

```bash
iproxy 2222 44
```

Ahora podrá habilitar *ssh* como root en *localhost* en el puerto 2222 y como contraseña `alpine`. Nota: si usó un jailbreak que no sea checkra1n, es posible que deba especificar un número de puerto diferente en lugar de 44. 

At this point you need to get access to the content of the device from your computer. One way is to run a command like `ssh root@localhost -p 2222 tar czf - /private > dump.tar.gz` which will save a tarball on the host of the */private/* folder from the phone. This will take a while.

En este punto, debe obtener acceso al contenido del dispositivo desde su computadora. Una forma es ejecutar un comando como `ssh root@localhost -p 2222 tar czf - /private > dump.tar.gz` que guardará un archivo en formato *tarball* en el host de la carpeta */private/* del teléfono. Esto tomará un tiempo.

Alternativamente, puede intentar ejecutar `sftp-server` para iOS y monte el sistema de archivos localmente usando `sshfs`.


## Utilizar `sshfs` en iOS

Si decide intentar usar sshfs, primero debe descargar localmente una copia compilada de sftp-server:

```bash
wget https://github.com/dweinstein/openssh-ios/releases/download/v7.5/sftp-server
```

Luego cargue el binario en el iPhone:

```bash
scp -P2222 sftp-server root@localhost:.
```

Deberá ingresar al dispositivo y establecer algunos derechos para permitir ejecutar `sftp-server` to run. Estos derechos se pueden copiar de un binario existente:

```bash
chmod +x sftp-server
ldid -e /binpack/bin/sh > /tmp/sh-ents
ldid -S /tmp/sh-ents sftp-server
```

Ahora puede crear una carpeta en el host y usarla como punto de montaje (**nota:** no cree esta carpeta en /tmp/): 

```bash
mkdir root_mount
sshfs -p 2222 -o sftp_server=/var/root/sftp-server root@localhost:/ root_mount
```
