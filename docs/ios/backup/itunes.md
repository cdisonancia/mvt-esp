# Copia de seguridad con la app iTunes

Es posible hacer una copia de seguridad de iPhone usando iTunes en computadoras Windows o Mac (en las versiones más recientes de Mac OS, esta función está incluida en Finder).

Para hacer eso:

* Asegúrate de que iTunes esté instalado.
* Conecta tu iPhone a tu computadora usando un cable Lightning / USB.
* Abre el dispositivo en iTunes (o Finder en Mac OS).
* Si deseas tener una detección más precisa, asegúrate de que la opción de copia de seguridad cifrada (*encrypted local backup*) esté activada y elije una contraseña segura para la copia de seguridad.
* Inicia la copia de seguridad y espera a que finalice (esto puede tardar hasta 30 minutos).

![](../../../img/macos-backup.jpg)
_Fuente: [Soporte de Apple](https://support.apple.com/en-us/HT211229)_

* Una vez realizada la copia de seguridad, busque su ubicación y cópiela en un lugar donde pueda ser analizada con `mvt`. En Windows, la copia de seguridad se puede almacenar en `%USER%\Apple\MobileSync\` o `%USER%\AppData\Roaming\Apple Computer\MobileSync\`. En Mac OS, la copia de seguridad se almacena en `~/Library/Application Support/MobileSync/`.
