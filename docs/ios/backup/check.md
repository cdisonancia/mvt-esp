# Verificar una copia de seguridad con mvt-ios

La copia de seguridad (**backup**) puede tardar algún tiempo. Es mejor asegurarse de que el teléfono permanezca desbloqueado durante este proceso. Posteriormente, se creará una nueva carpeta en la ruta que especificó utilizando el UDID del iPhone del que realizó la copia de seguridad. 

## Extraer y guardar la clave de descifrado (opcional) 

Si no desea ingresar una contraseña cada vez que descifra una copia de seguridad, en su lugar, MVT puede aceptar un archivo de clave. Esta clave se puede utilizar con el comando `decrypt-backup`.

Para generar un archivo de clave, necesitará la copia de seguridad de su dispositivo y la contraseña de copia de seguridad:

    $ mvt-ios extract-key --help
    Usage: mvt-ios extract-key [OPTIONS] BACKUP_PATH

      Extract decryption key from an iTunes backup

    Options:
      -p, --password TEXT  Password to use to decrypt the backup  [required]
      -k, --key-file FILE  Key file to be written (if unset, will print to STDOUT)
      --help               Show this message and exit.

You can specify the password on the command line, or omit the `-p` option to have MVT prompt for a password. The `-k` option specifies where to save the file containing the decryption key. If `-k` is omitted, MVT will display the decryption key without saving.

Puede especificar la contraseña en la línea de comando u omitir la opción `-p` para que MVT solicite una contraseña. La opción `-k` especifica dónde guardar el archivo que contiene la clave de descifrado. Si `-k` se omite, MVT mostrará la clave de descifrado sin guardar. 


_Nota_: ¡Esta clave de descifrado es información confidencial! Mantenga el archivo seguro.

Para extraer la clave y que MVT solicite una contraseña: 

```bash
mvt-ios extract-key -k /path/to/save/key /path/to/backup
```

## Descifrando una copia de seguridad 

En caso de que tenga una copia de seguridad cifrada, primero deberá descifrarla.  Esto también se puede hacer con `mvt-ios`:

    $ mvt-ios decrypt-backup --help
    Usage: mvt-ios decrypt-backup [OPTIONS] BACKUP_PATH

      Decrypt an encrypted iTunes backup

    Options:
      -d, --destination TEXT  Path to the folder where to store the decrypted
                              backup  [required]

      -p, --password TEXT     Password to use to decrypt the backup (or, set
                              MVT_IOS_BACKUP_PASSWORD environment variable)
                              NOTE: This argument is mutually exclusive with
                              arguments: [key_file].

      -k, --key-file PATH     File containing raw encryption key to use to decrypt
                              the backup NOTE: This argument is mutually exclusive
                              with arguments: [password].

      --help                  Show this message and exit.

You can specify the password in the environment variable `MVT_IOS_BACKUP_PASSWORD`, or via command-line argument, or you can pass a key file.  You need to specify a destination path where the decrypted backup will be stored. If a password cannot be found and no key file is specified, MVT will ask for a password. Following is an example usage of `decrypt-backup` sending the password via an environment variable:

Puede especificar la contraseña en la variable de entorno `MVT_IOS_BACKUP_PASSWORD`, mediante el argumento de la línea de comandos o puede indicar un archivo de clave. Debe especificar una ruta de destino donde se almacenará la copia de seguridad descifrada. Si no se puede encontrar una contraseña y no se especifica ningún archivo de clave, MVT le pedirá una contraseña. A continuación se muestra un ejemplo de uso de `decrypt-backup` indicando la contraseña a través de una variable de entorno: 

```bash
MVT_IOS_BACKUP_PASSWORD="mypassword" mvt-ios decrypt-backup -d /path/to/decrypted /path/to/backup
```

## Ejecutar `mvt-ios` en una copia de seguridad 

Una vez que tenga una copia de seguridad descifrada disponible para su análisis, puede utilizar el subcomando `check-backup`:

    $ mvt-ios check-backup --help
    Usage: mvt-ios check-backup [OPTIONS] BACKUP_PATH

      Extract artifacts from an iTunes backup

    Options:
      -i, --iocs PATH     Path to indicators file
      -o, --output PATH   Specify a path to a folder where you want to store JSON
                          results

      -f, --fast          Avoid running time/resource consuming features
      -l, --list-modules  Print list of available modules and exit
      -m, --module TEXT   Name of a single module you would like to run instead of
                          all

      --help              Show this message and exit.

A continuación se muestra un uso básico de `check-backup`:

```bash
mvt-ios check-backup --output /path/to/output/ /path/to/backup/udid/
```

Este comando creará algunos archivos JSON que contienen los resultados de la extracción. Si no especifica una opción de salida con `--output`, `mvt-ios` simplemente procesará los datos sin almacenar los resultados en el disco. 

A través del argumento `--iocs` puede indicar un archivo [STIX2](https://oasis-open.github.io/cti-documentation/stix/intro) que defina una lista de indicadores maliciosos para revisar los registros extraídos desde la copia de seguridad por mvt. Cualquier coincidencia se resaltará en la salida del terminal y se guardará en la carpeta de salida con el sufijo "*_detected*" en el nombre del archivo JSON. 