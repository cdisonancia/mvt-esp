# Copia de seguridad con libimobiledevice

Si has [instalado libimobiledevice](../install.md) correctamente, puedes generar fácilmente una copia de seguridad de iTunes utilizando la herramienta idevicebackup2 incluida en la suite. Primero, es posible que desees asegurarte de que el cifrado de la copia de seguridad esté habilitado (**nota: la copia de seguridad cifrada contiene más datos que las copias de seguridad no cifradas**):

```bash
idevicebackup2 -i backup encryption on
```

Note that if a backup password was previously set on this device, you might need to use the same or change it. You can try changing password using `idevicebackup2 -i backup changepw`, 


Ten en cuenta que si se estableciste previamente una contraseña de respaldo en este dispositivo, es posible que debas usar la misma o cambiarla. Puedes intentar cambiar la contraseña usando `idevicebackup2 -i backup changepw` o desactivando el cifrado (`idevicebackup2 -i backup encryption off`) y volviéndolo a activar.

Si no puede recuperar o cambiar la contraseña, debe intentar deshabilitar el cifrado y obtener una copia de seguridad sin cifrar.

If all else fails, as a *last resort* you can try resetting the password by [resetting all the settings through the iPhone's Settings app](https://support.apple.com/en-us/HT205220), via `Settings » General » Reset » Reset All Settings`.  Note that resetting the settings through the iPhone's Settings app will wipe some of the files that contain useful forensic traces, so try the options explained above first.

Si todo lo demás falla, como *último recurso*, puede intentar restablecer la contraseña [reseteando todas las configuraciones a través de la Aplicación de Configuración del iPhone](https://support.apple.com/en-us/HT205220), a través de `Settings » General » Reset » Reset All Settings`. Tenga en cuenta que restablecer la configuración a través de la Aplicación de Configuración del iPhone borrará algunos de los archivos que contienen rastros forenses útiles, así que pruebe primero las opciones explicadas anteriormente. 

Una vez que esté listo, puedes continuar realizando la copia de seguridad:

```bash
idevicebackup2 backup --full /path/to/backup/
```
