# Metodología forense de iOS

Antes de comenzar a adquirir y analizar datos de un dispositivo iOS, debes evaluar cuál es tu plan de acción preciso. Dado que dispones de múltiples opciones, debes definir y familiarizarste con la metodología forense más eficaz en cada caso.

#### Volcado del sistema de archivos (Filesystem Dump)

Debes decidir si harás [*jailbreak*](https://es.wikipedia.org/wiki/Jailbreak_(iOS)) al dispositivo y obtener un volcado completo del sistema de archivos (*filesystem dump*), o no.

Si bien el acceso al sistema de archivos completo permite extraer datos que de otro modo no estarían disponibles, es posible que no siempre sea posible hacer jailbreak a un determinado modelo de iPhone o versión de iOS. Además, según el tipo de jailbreak disponible, hacerlo podría comprometer algunos registros importantes, contaminar otros o causar posteriormente un mal funcionamiento no intencionado del dispositivo en caso de que se vuelva a utilizar.

Si no se espera utilizar el teléfono nuevamente, es posible que desee considerar intentar un jailbreak después de haber agotado todas las demás opciones, incluida una copia de seguridad (*Backup*).

#### Copia de seguridad de iTunes

An alternative option is to generate an iTunes backup (in most recent version of mac OS, they are no longer launched from iTunes, but directly from Finder). While backups only provide a subset of the files stored on the device, in many cases it might be sufficient to at least detect some suspicious artifacts. Backups encrypted with a password will have some additional interesting records not available in unencrypted ones, such as Safari history, Safari state, etc.

Una opción alternativa es generar una copia de seguridad de iTunes (en la versión más reciente de MAC OS, ya no se inician desde iTunes, sino directamente desde Finder). Si bien las copias de seguridad solo proporcionan un subconjunto de los archivos almacenados en el dispositivo, en muchos casos puede ser suficiente para al menos detectar algunos elementos sospechosos. Las copias de seguridad cifradas con una contraseña tendrán algunos registros interesantes adicionales que no están disponibles en los no cifrados, como el historial de Safari, el estado de Safari, etc.
