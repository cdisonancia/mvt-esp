<p align="center">
     <img src="./docs/mvt.png" width="300" />
</p>


# Traducción de documentación de Progarama MVT

Esta es una traducción al español de la documentación del programa MVT (*Mobile Verification Toolkit*), el cual es una herramienta de **Análisis Forense Consentido** para detectar Pegasus en sistemas móviles; iOS y Android.

La traducción está disponible aquí: https://cdisonancia.gitlab.io/mvt-esp

## MVT
El repositorio del MVT se encuentra en https://github.com/mvt-project/mvt y su documentación oficial aquí: https://docs.mvt.re/en/latest/

## Proyecto Pegasus
MVT es el resultado de una investigación publicada por Amnistía internacional sobre la evidencia existente de ataques de Pegasus, el software espía de NSO Group. La publicación puede ser consultada aquí: 

[Forensic Methodology Report: How to catch NSO Group’s Pegasus (2021)](https://www.amnesty.org/en/latest/research/2021/07/forensic-methodology-report-how-to-catch-nso-groups-pegasus/)

Los indicadores de compromiso de infección con Pegasus se encuentran disponibles en el [repositorio de Amnistía Internacional](https://github.com/AmnestyTech/investigations/tree/master/2021-07-18_nso).

## Licencia
MVT Usa una licencia propia, derivada de la Licencia Pública Mozilla v2.0, que restringe el uso del programa para usos forenses no consentido. La explicación general del propósito de esta condición [se encuentra en la traducción](https://cdisonancia.gitlab.io/mvt-esp/license/) y la licencia completa está disponible [aquí](https://github.com/mvt-project/mvt/blob/main/LICENSE).

## Publicación
Este repositorio y la traducción se publicaron junto a una publicación sobre los nuevos antecedentes de Pegasus y una reflexión política al respecto: https://colectivodisonancia.net/pegasus


